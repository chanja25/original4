#include	<windows.h>

//
//		カーソル座標移動
//

void	SetPos( int x, int y )
{
	HANDLE	a;
	COORD	b;

	b.X = x;
	b.Y = y;
	a = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition( a, b ); 
}

//
//		テキストアトリビュート変更
//

void	SetAtr( int fore, int back )
{
	HANDLE	a;
	WORD	atr = 0;

	if( fore & 0x01 ) atr |= FOREGROUND_RED;
	if( fore & 0x02 ) atr |= FOREGROUND_GREEN;
	if( fore & 0x04 ) atr |= FOREGROUND_BLUE;
	if( fore & 0x08 ) atr |= FOREGROUND_INTENSITY;

	if( back & 0x01 ) atr |= BACKGROUND_RED;
	if( back & 0x02 ) atr |= BACKGROUND_GREEN;
	if( back & 0x04 ) atr |= BACKGROUND_BLUE;
	if( back & 0x08 ) atr |= BACKGROUND_INTENSITY;

	a = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute( a, atr ); 
}

//
//
//

void	cursor( int on )
{
	HANDLE	a;
	CONSOLE_CURSOR_INFO	cci;

	cci.dwSize = 1;
	if( on ) cci.bVisible = TRUE; else cci.bVisible = FALSE;

	a = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorInfo( a, &cci);
}
